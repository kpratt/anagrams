package anagrams

import (
	"testing"
	"strings"
)

func TestBuildTrie(t *testing.T) {

	c := make(chan []rune, 5)
	c <- []rune("aaa")
	c <- []rune("aab")
	c <- []rune("baa")
	c <- []rune("dad")
	close(c)

	trie := BuildTrie(c)

	var ok bool
	if a_node, ok := trie.At('a'); ok {
		if aa_node, ok := a_node.At('a'); ok {
			if aab_node, ok := aa_node.At('b'); ok {
				if !aab_node.IsTerminal() {
					t.Error("aab wasn't terminal")
				}
			} else {
				t.Error("Missing 'aab' node.")
			}
		} else {
			t.Error("Missing 'aa' node.")
		}
	} else {
		t.Error("Missing 'a' node.")
	}
	if b, ok := trie.At('b'); ok {
		if ba, ok := b.At('a'); ok {
			if baa, ok := ba.At('a'); ok {
				if !baa.IsTerminal() {
					t.Error("baa wasn't terminal!")
				}
			} else {
				t.Error("Missing 'baa' node.")
			}
		} else {
			t.Error("Missing 'ba' node.")
		}

	} else {
		t.Error("Missing 'b' node.")
	}
	if _, ok = trie.At('d'); !ok {
		t.Error("Missing 'd' node.")
	}

}

func TestTrieSearch(t *testing.T) {
	c := make(chan []rune, 5)
	c <- []rune("aazz")
	c <- []rune("aab")
	c <- []rune("baa")
	c <- []rune("dad")
	c <- []rune("abazz")
	close(c)

	trie := BuildTrie(c)

	results := TrieSearch(trie, []rune("abazzz"))

	if len(results) != 4 {
		t.Error("Only got ", len(results), " of 2 expected results.")
		return
	}

	//for _, rs := range results {
	//	fmt.Println(string(rs))
	//}

	if strings.Compare(string(results[0]), "abazz") != 0 {
		t.Error("aab not found. got ", string(results[0]))
	}

	if strings.Compare(string(results[1]), "aab") != 0 {
		t.Error("baa not found. got '", string(results[1]), "'")
	}

}

func TestRemoveAt(t *testing.T) {
	rs := []rune{'a', 'b', 'c'}
	removeAt(0, rs)
	if len(rs) != 3 {
		t.Error("remove at zero caused mutation.")
	}
	removeAt(2, rs)
	if len(rs) != 3 {
		t.Error("remove at two caused mutation.")
	}
	removeAt(1, rs)
	if len(rs) != 3 {
		t.Error("remove at middle caused mutation.")
	}

}
