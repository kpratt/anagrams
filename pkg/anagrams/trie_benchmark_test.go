package anagrams

import (
	"testing"
	"path/filepath"
	"log"
	"fmt"
)

func BenchmarkTrieSearch(b *testing.B) {
	// run the Fib function b.N times
	absPath, err := filepath.Abs(dictPath)
	if err != nil {
		log.Fatal(err.Error())
	}

	c := make(chan []rune)
	go func() {
		ReadDictionary(absPath, c)
		close(c)
	}()
	trie := BuildTrie(c)

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		results = TrieSearch(trie, letters)
	}
	b.StopTimer()
	fmt.Println("Trie result count = ", len(results))
}
