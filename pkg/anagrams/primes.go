package anagrams

import (
	"log"
	"math"
)

type Dictionary map[int][][]rune

func FactorSearch(d Dictionary, letterset int) (results [][]rune) {
	results = make([][]rune, 0, 32)
	for k, vs := range d {
		if letterset%k == 0 {
			results = append(results, vs...)
		}
	}
	return
}

func SuperFactorSearch(alpha map[rune]int, d Dictionary, letterset []rune) (results [][]rune) {
	results = make([][]rune, 0, 32)
	for i:=1; i<int(math.Pow(2, float64(len(letterset)))); i++ {
		y:=1
		x:=i
		for j:=0; x!=0; j++ {
			if x & 0x1 != 0 {
				y = y * alpha[letterset[j]]
			}
			x = x >> 1
		}
		if words, ok := d[y]; ok {
			results = append(results, words...)
		}
	}
	return
}


func BuildAlphabet(rs []rune) (ret map[rune]int) {
	primes := make([]int, 1, len(rs))
	primes[0] = 2

loop:
	for x := primes[len(primes)-1] + 1; len(primes) <= len(rs); x += 2 {
		for p := 0; p < len(primes); p++ {
			if x%primes[p] == 0 {
				continue loop
			}
		}
		primes = append(primes, x)
	}

	ret = make(map[rune]int)
	for i := 0; i < len(rs); i++ {
		ret[rs[i]] = primes[i]
	}
	return
}

func FactorProduct(alpha map[rune]int, rs []rune) int {
	x := 1
	for _, r := range rs {
		if y, ok := alpha[r]; ok {
			x = x * y
		}
	}
	return x
}

func BuildDictionary(alpha map[rune]int) (func(<-chan []rune) Dictionary) {
	return func(c <-chan []rune) (m Dictionary) {
		m = make(map[int][][]rune)
		for rs := range c {
			x := FactorProduct(alpha, rs)
			if x == 0 {
				log.Fatal(string(rs), " had zero-product.")
			}
			if _, ok := m[x]; !ok {
				m[x] = make([][]rune, 0, 1)
			}
			m[x] = append(m[x], rs)
		}
		return
	}
}
