package anagrams

type TrieVisitor interface {
	VisitTerminal(path []rune)
	VisitSplit(trav TraverseTrie)
}

type TraverseTrie struct {
	trie Trie
	path []rune
	pLen int
}

func NewTraversal(trie Trie) TraverseTrie {
	return TraverseTrie{
		trie: trie,
		path: make([]rune, 0, 16),
		pLen: 0,
	}
}

func (trav TraverseTrie) Path() []rune {
	return trav.path[:trav.pLen]
}

func (trav TraverseTrie) Visit(r rune, visitor TrieVisitor) {
	if sub, ok := trav.trie[r]; ok {

		path2 := append(trav.path, r)
		tr2 := TraverseTrie{
			trie: sub,
			path: path2,
			pLen: trav.pLen + 1,
		}

		//fmt.Println("Visiting      '" + string(tr2.Path()), "'")
		if sub.IsTerminal() {
			visitor.VisitTerminal(tr2.Path())
		} else {
			//fmt.Println("'", string(tr2.Path()), "' was not terminal.")
		}

		visitor.VisitSplit(tr2)
		//fmt.Println("Backtracking  " + string(tr2.Path()))
	}

}

type Trie map[rune]Trie

func (t Trie) At(r rune) (s Trie, ok bool) {
	s, ok = t[r]
	return
}

func (t Trie) IsTerminal() (ok bool) {
	_, ok = t[0]
	return
}

func (t Trie) Insert(rs []rune) {
	if len(rs) > 0 {
		if sub, ok := t.At(rs[0]); ok {
			sub.Insert(rs[1:])
		} else {
			t[rs[0]] = make(map[rune]Trie)
			t[rs[0]].Insert(rs[1:])
		}
	} else {
		t[0] = nil
	}
}

type AnagramVisitor struct {
	results chan<- []rune
	letters []rune
}

func (v AnagramVisitor) VisitTerminal(path []rune) {
	tmp := make([]rune, len(path))
	copy(tmp, path)
	//fmt.Println("adding to results: \"", string(tmp), "\"")
	v.results <- tmp
}
func (v AnagramVisitor) VisitSplit(trav TraverseTrie) {
	visited := make(map[rune]bool)
	for i, r := range v.letters {
		if _, ok := visited[r]; !ok {
			visited[r] = true
			trav.Visit(r, AnagramVisitor{
				results: v.results,
				letters: removeAt(i, v.letters),
			})
		}
	}
}

func removeAt(i int, ls []rune) (ret []rune) {

	ret = make([]rune, len(ls)-1)
	if i < 0 || i >= len(ls) {
		return []rune{}
	}

	if i == 0 {
		copy(ret, ls[1:])
		return
	}

	if i == len(ls)-1 {
		copy(ret, ls[:len(ls)-1])
		return
	}

	copy(ret, ls[:i])
	ret = append(ret, ls[i+1:]...)
	return
}

func BuildTrie(c <-chan []rune) (t Trie) {
	t = make(map[rune]Trie)
	for rs := range c {
		t.Insert(rs)
	}
	return
}

func TrieSearch(t Trie, letterset []rune) (results [][]rune) {
	c := make(chan []rune)
	visitor := AnagramVisitor{
		results: c,
		letters: letterset,
	}
	traverser := NewTraversal(t)
	go func() {
		visitor.VisitSplit(traverser)
		close(c)
	}()

	results = make([][]rune, 0, 32)
	for rs := range c {
		results = append(results, rs)
	}

	return
}
