package anagrams

import (
	"testing"
	"path/filepath"
	"log"
	"fmt"
)

var letters = []rune("aatpberst")
var dictPath = "../../dict/english/words_alpha.txt"
var results [][]rune

func BenchmarkFactorSearch(b *testing.B) {
	// run the Fib function b.N times
	absPath, err := filepath.Abs(dictPath)
	if err != nil {
		log.Fatal(err.Error())
	}


	c := make(chan []rune)
	go func() {
		ReadDictionary(absPath, c)
		close(c)
	}()
	dictionary := BuildDictionary(alpha)(c)

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		results = FactorSearch(dictionary, FactorProduct(alpha, letters))
	}
	b.StopTimer()
	fmt.Println("Primes result count = ", len(results))
}

func BenchmarkSuperFactorSearch(b *testing.B) {
	// run the Fib function b.N times
	absPath, err := filepath.Abs(dictPath)
	if err != nil {
		log.Fatal(err.Error())
	}


	c := make(chan []rune)
	go func() {
		ReadDictionary(absPath, c)
		close(c)
	}()
	dictionary := BuildDictionary(alpha)(c)

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		results = SuperFactorSearch(alpha, dictionary, letters)
	}
	b.StopTimer()
	fmt.Println("Primes result count = ", len(results))
}
