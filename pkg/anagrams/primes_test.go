package anagrams

import (
	"testing"
	"strings"
)

var alpha = BuildAlphabet([]rune{
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
	'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'})

func TestAlpha(t *testing.T) {
	if alpha['a'] != 2 {
		t.Error("Alpha['a'] = ", alpha['a'])
	}

	if alpha['b'] != 3 {
		t.Error("Alpha['b'] = ", alpha['b'])
	}

	if alpha['c'] != 5 {
		t.Error("Alpha['c'] = ", alpha['c'])
	}

	if alpha['z'] != 101 {
		t.Error("Alpha['z'] = ", alpha['z'])
	}
}

func TestBuildDictionary(t *testing.T) {
	builder := BuildDictionary(alpha)

	c := make(chan []rune, 5)
	c <- []rune("aaa")
	c <- []rune("aab")
	c <- []rune("baa")
	c <- []rune("dad")
	c <- []rune("sad")
	close(c)

	dictionary := builder(c)

	aaa, ok := dictionary[8]
	if !ok {
		t.Error("aaa was not found")
	} else if len(aaa) == 0 {
		t.Error("aaa was empty")
	} else if strings.Compare(string(aaa[0]), "aaa") != 0 {
		t.Error("aaa had wrong value ", aaa[0])
	}

	aab, ok := dictionary[12]
	if !ok {
		t.Error("aab was not found")
	} else if len(aab) != 2 {
		t.Error("aab entry(ies) were missing")
	} else {
		if strings.Compare(string(aab[0]), "aab") != 0 {
			t.Error("aaa had wrong value ", aaa[0])
		}

		if strings.Compare(string(aab[1]), "baa") != 0 {
			t.Error("aaa had wrong value ", aaa[0])
		}
	}

}

func TestFactorSearch(t *testing.T) {
	builder := BuildDictionary(alpha)

	c := make(chan []rune, 5)
	c <- []rune("aaa")
	c <- []rune("aab")
	c <- []rune("baa")
	c <- []rune("dad")
	c <- []rune("sad")
	close(c)

	dictionary := builder(c)

	results := FactorSearch(dictionary, FactorProduct(alpha, []rune("aba")))

	if len(results) != 2 {
		t.Error("Only got ", len(results), " of 2 expected results.")
	}

	if strings.Compare(string(results[0]), "aab") != 0 {
		t.Error("aab not found.")
	}

	if strings.Compare(string(results[1]), "baa") != 0 {
		t.Error("baa not found.")
	}

}
